import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {
    ActivatedRouteSnapshot,
    Resolve,
    Router,
    RouterStateSnapshot
} from '@angular/router';

import { JiraApplicationInterface } from '../interfaces/JiraApplicationInterface';
import { JiraApplicationService } from '../services/JiraApplicationService';

@Injectable()
export class JiraApplicationsResolver implements Resolve<JiraApplicationInterface> {
    constructor(private jiraApplicationsService: JiraApplicationService, private router: Router) {}

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<JiraApplicationInterface> {
        return new Promise((resolve) => {
            return new Promise((resolve) => {
                this.jiraApplicationsService.loadJiraApplications().subscribe((jiraApplications) => {
                    resolve(jiraApplications);
                });
            });
        });
    }
}
