import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {
    ActivatedRouteSnapshot,
    Resolve,
    Router,
    RouterStateSnapshot
} from '@angular/router';

import { JiraUserInterface } from '../interfaces/JiraUserInterface';
import { JiraUserService } from '../services/JiraUserService';

@Injectable()
export class JiraUsersResolver implements Resolve<JiraUserInterface> {
    constructor(private jiraUserService: JiraUserService, private router: Router) {}

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<JiraUserInterface> {
        return new Promise((resolve) => {
            const subscription = this.jiraUserService.loadJiraUsers(route.params['applicationId']).subscribe((jiraUsers) => {
                subscription.unsubscribe();
                resolve(jiraUsers);
            });
        });
    }
}
