import { Component } from '@angular/core';

@Component({
  selector: 'application-component',
})
export class ApplicationComponent {
  title = 'app works!';
}
