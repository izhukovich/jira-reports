import {Component, Input, OnInit} from '@angular/core';
import {JiraApplicationInterface} from '../interfaces/JiraApplicationInterface';
import {JiraApplicationService} from '../services/JiraApplicationService';

@Component({
    selector: 'applications-list',
    templateUrl: '../templates/components/applications_list.html'
})
export class JiraApplicationsListComponent implements OnInit {
    @Input() public jiraApplications: JiraApplicationInterface[] = [];

    public constructor(
        private jiraApplicationService: JiraApplicationService,
    ) {}

    public ngOnInit() {
        this.subscribeToJiraApplications();
    }

    private subscribeToJiraApplications() {
        this.jiraApplications = this.jiraApplicationService.jiraApplications;
        this.jiraApplicationService.onLoadJiraApplications$.subscribe((jiraApplications: JiraApplicationInterface[]) => {
            this.jiraApplications = jiraApplications;
        });
    }
}