import { Component, Input, OnInit } from '@angular/core';
import { AccountService } from '../services/AccountService';
import { AccountInterface } from '../interfaces/AccountInterface';

@Component({
    selector: 'application-account',
    templateUrl: '../templates/components/application_account.html'
})
export class AccountComponent implements OnInit {
    @Input() public account: AccountInterface;
    public loading: boolean = true;

    public constructor(
        private accountService: AccountService,
    ) {}

    public ngOnInit() {
        this.subscribeAccount();
        this.accountService.loadAccount();
    }

    private subscribeAccount() {
        this.account = this.accountService.account;
        this.accountService.onLoadAccount$.subscribe((account: AccountInterface) => {
            this.account = account;
            console.log(account);
            this.loading = false;
        });
    }
}