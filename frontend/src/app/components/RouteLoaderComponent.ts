import { Component } from '@angular/core';

import 'rxjs/add/operator/debounceTime';

import {
    Event as RouterEvent,
    NavigationCancel,
    NavigationEnd,
    NavigationError,
    NavigationStart,
    RoutesRecognized,
    Router
} from '@angular/router';

@Component({
    selector: 'route-loader',
    templateUrl: '../templates/components/route_loader.html'
})
export class RouteLoaderComponent {
    public loading: boolean = true;

    constructor(private router: Router) {
        router.events.debounceTime(50).subscribe((event: RouterEvent) => {
            this.navigationInterceptor(event);
        });
    }

    private navigationInterceptor(event: RouterEvent): void {
        if (event instanceof NavigationStart) {
            this.loading = true;
        }
        if (event instanceof RoutesRecognized) {
            this.loading = true;
        }
        if (event instanceof NavigationEnd) {
            setTimeout(() => { this.loading = false; }, 100);
        }
        if (event instanceof NavigationCancel) {
            setTimeout(() => { this.loading = false; }, 100);
        }
        if (event instanceof NavigationError) {
            setTimeout(() => { this.loading = false; }, 100);
        }
    }
}