import {Component, Input, OnInit} from '@angular/core';
import { JiraUserInterface } from '../interfaces/JiraUserInterface';
import { JiraUserService } from '../services/JiraUserService';

@Component({
    selector: 'users-list',
    templateUrl: '../templates/components/users_list.html'
})
export class JiraUsersListComponent implements OnInit {
    @Input() public jiraUsers: JiraUserInterface[] = [];

    public constructor(
        private jiraUserService: JiraUserService,
    ) {}

    public ngOnInit() {
        this.subscribeToJiraUsers();
    }

    private subscribeToJiraUsers() {
        this.jiraUsers = this.jiraUserService.jiraUsers;
        this.jiraUserService.onLoadJiraUsers$.subscribe((jiraUsers: JiraUserInterface[]) => {
            this.jiraUsers = jiraUsers;
        });
    }
}