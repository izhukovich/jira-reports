import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { CookieModule } from 'ngx-cookie';

import { RoutingModule } from './RoutingModule';

import { RouteLoaderComponent } from '../components/RouteLoaderComponent';
import { ApplicationController } from '../controllers/ApplicationController';
import { JiraApplicationService } from '../services/JiraApplicationService';
import { ConfigService } from '../services/ConfigService';
import { ApiService } from '../services/ApiService';
import { JiraUserService } from '../services/JiraUserService';
import { ReportService } from '../services/ReportService';
import { AccountService } from '../services/AccountService';

@NgModule({
  bootstrap: [ ApplicationController ],
  declarations: [
    ApplicationController,
    RouteLoaderComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    RoutingModule,
    CookieModule.forRoot(),
  ],
  providers: [
    JiraApplicationService,
    JiraUserService,
    ReportService,
    AccountService,
    ConfigService,
    ApiService
  ]
})
export class ApplicationModule { }