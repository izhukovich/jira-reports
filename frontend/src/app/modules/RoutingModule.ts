import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { SharedModule } from './SharedModule';
import { ApplicationRoutes } from '../config/ApplicationRoutes';
import { RouterModule } from '@angular/router';
import { IndexController } from '../controllers/IndexController';
import { JiraApplicationsListComponent } from '../components/JiraApplicationsListComponent';
import { JiraApplicationsResolver } from '../resolvers/JiraApplicationsResolver';
import { ReportController } from '../controllers/ReportController';
import { ForbiddenController } from '../controllers/ForbiddenController';
import { JiraUsersListComponent } from '../components/UsersListComponent';
import { JiraUsersResolver } from '../resolvers/JiraUsersResolver';
import { AccountComponent } from '../components/AccountComponent';

@NgModule({
    declarations: [
        IndexController,
        ReportController,
        ForbiddenController,
        JiraApplicationsListComponent,
        JiraUsersListComponent,
        AccountComponent
    ],
    exports: [
        RouterModule,
        AccountComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        RouterModule.forRoot(ApplicationRoutes)
    ],
    providers: [
        JiraApplicationsResolver,
        JiraUsersResolver
    ]
})
export class RoutingModule {}