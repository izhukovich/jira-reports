import { NgModule } from '@angular/core';
import { DatepickerModule } from 'ng2-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [],
    imports: [
        DatepickerModule.forRoot(),
        FormsModule
    ],
    exports: [
        DatepickerModule,
        FormsModule
    ]
})
export class SharedModule {}