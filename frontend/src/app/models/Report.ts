import { ReportInterface } from '../interfaces/ReportInterface';

export class Report implements ReportInterface {
    public key: string;
    public summary: string;
    public assignee: any[];
    public description: string;
    public estimated_time: number;
    public time_spent: number;
    public status: any[];
    public updated_at: string;
    public created_at: string;
    public resolved: string;

    public constructor(params: any) {
        Object.assign(this, params);
    }

    public serialize(): any {}

}