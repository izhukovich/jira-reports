import { JiraApplicationInterface } from '../interfaces/JiraApplicationInterface';

export class JiraApplication implements JiraApplicationInterface {
    public id: number;
    public name: string;


    public constructor(params: any) {
        Object.assign(this, params);
    }

    public serialize(): any {}

}