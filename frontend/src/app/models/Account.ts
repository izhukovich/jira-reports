import { AccountInterface } from '../interfaces/AccountInterface';

export class Account implements AccountInterface {
    public name: string;
    public role: string;

    public constructor(params: any) {
        Object.assign(this, params);
    }

    public serialize(): any {}

}