import { JiraUserInterface } from '../interfaces/JiraUserInterface';

export class JiraUser implements JiraUserInterface {
    public name: string;
    public emailAddress: string;
    public avatarUrls: any[];
    public displayName: string;
    public timeZone: string;

    public constructor(params: any) {
        Object.assign(this, params);
    }

    public serialize(): any {}

}