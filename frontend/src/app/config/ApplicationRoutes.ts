import { Route } from '@angular/router';

import { IndexController } from '../controllers/IndexController';
import { ReportController } from '../controllers/ReportController';
import { ForbiddenController } from '../controllers/ForbiddenController';

import { JiraApplicationsResolver } from '../resolvers/JiraApplicationsResolver';
import { JiraUsersResolver } from '../resolvers/JiraUsersResolver';

/**
 * Application routes
 */
export const ApplicationRoutes: Route[] = [
    {
        path: '',
        component: IndexController
    },
    {
        path: 'report/:applicationId',
        component: ReportController,
        resolve: {
            jiraUsers: JiraUsersResolver,
        }
    },
    {
        path: 'forbidden',
        component: ForbiddenController,
    },
    {
        path: '**',
        component: IndexController
    }
];