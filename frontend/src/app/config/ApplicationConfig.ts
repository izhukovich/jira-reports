import { ApiConfig } from '../config/ApiConfig';
import { ApplicationRoutes } from '../config/ApplicationRoutes';
import { UrlsConfig } from '../config/UrlsConfig';

/**
 * Application configuration
 * @type {{[string]: any}}
 */
export const ApplicationConfig = {
    titles: {
        main: 'Home',
        applications: 'Applications',
    },
    api: ApiConfig,
    urls: UrlsConfig
};