const baseUrl = 'http://localhost:8080';

/**
 * Api endpoint settings
 * @type {{[string]: string}}
 */
export const ApiConfig = {
    jiraApplications: `${baseUrl}/api/applications`,
    jiraUsers: `${baseUrl}/api/users/{applicationId}`,
    reports: `${baseUrl}/api/issues/{applicationId}`,
    account: `${baseUrl}/api/account`,
};