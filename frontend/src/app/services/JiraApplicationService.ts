import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { ApiService } from './ApiService';
import { JiraApplicationInterface } from '../interfaces/JiraApplicationInterface';
import { JiraApplicationsFilterInterface } from '../interfaces/JiraApplicationsFilterInterface';
import { JiraApplication } from '../models/JiraApplication';
import { ConfigService } from "./ConfigService";

@Injectable()
export class JiraApplicationService {
    /**
     * @type {Subject<JiraApplicationInterface[]>}
     */
    private onLoadJiraApplicationsSubject$: Subject<JiraApplicationInterface[]> = new Subject<JiraApplicationInterface[]>();

    /**
     * @type {Subject<JiraApplicationsFilterInterface>}
     */
    private onJiraApplicationsFiltersChangeSubject$: Subject<JiraApplicationsFilterInterface> = new Subject<JiraApplicationsFilterInterface>();

    /**
     * @type {JiraApplicationInterface[]}
     */
    private $jiraApplications: JiraApplicationInterface[] = [];

    /**
     * @type {JiraApplicationsFilterInterface}
     */
    private $jiraApplicationsFilter: JiraApplicationsFilterInterface;

    /**
     * @type {JiraApplicationsFilterInterface}
     */
    private $jiraApplicationsFilterDefault: JiraApplicationsFilterInterface;

    /**
     * @type {Observable<JiraApplicationInterface[]>}
     */
    public onLoadJiraApplications$: Observable<JiraApplicationInterface[]> = this.onLoadJiraApplicationsSubject$.asObservable();

    /**
     * Set initial configuration values
     * @param {ConfigService} configService
     * @param {ApiService} apiService
     */
    public constructor(
        private configService: ConfigService,
        private apiService: ApiService
    ) {
        this.$jiraApplicationsFilter = this.$jiraApplicationsFilterDefault = { };
    }

    public loadJiraApplications(
        filter: boolean = false
    ): Observable<JiraApplicationInterface[]> {
        let options: JiraApplicationsFilterInterface = this.$jiraApplicationsFilterDefault;
        if (filter) {
            options = this.$jiraApplicationsFilter;
        }

        this.apiService.loadJiraApplications(options).subscribe(
            ({ data, error }: { data: any[], error: boolean }) => {
                const array: JiraApplicationInterface[] = [];

                if (error) {
                    return;
                }

                data.forEach((element) => {
                    array.push(new JiraApplication(element));
                });

                this.$jiraApplications = array;
                this.onLoadJiraApplicationsSubject$.next(this.$jiraApplications);
            }
        );

        return this.onLoadJiraApplications$;
    }

    /**
     * Set new jira applications filter, or replace an existing one
     * @param {string|any} name Parameter name
     * @param {any} value Parameter value
     */
    public setJiraApplicationsFilter(name: string|any, value: any = null) {


        this.onJiraApplicationsFiltersChangeSubject$.next(this.$jiraApplicationsFilter);
    }

    /**
     * Reset jira applications filter to defaults
     */
    public resetJiraApplicationsFilter() {
        this.$jiraApplicationsFilter = this.$jiraApplicationsFilterDefault;
    }

    /**
     * Get jira applications list
     * @returns {JiraApplicationInterface[]}
     */
    get jiraApplications(): JiraApplicationInterface[] {
        return this.$jiraApplications;
    }

    /**
     * Get current jira applications filter
     * @returns {JiraApplicationsFilterInterface}
     */
    get jiraApplicationsFilters(): JiraApplicationsFilterInterface {
        return this.$jiraApplicationsFilter;
    }
}