import { Injectable } from '@angular/core';

import { ApplicationConfig } from '../config/ApplicationConfig';

@Injectable()
export class ConfigService {
    private configuration = ApplicationConfig;
    private env: string = 'development';

    public getConfig() {
        return this.configuration;
    }

    public environment(env: string = null): string {
        if (env) {
            this.env = env;
        } else {
            return this.env;
        }
    }

    public getUrls(): any {
        return this.configuration.urls[this.env];
    }
}