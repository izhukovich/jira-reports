import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { ApiService } from './ApiService';
import { JiraUserInterface } from '../interfaces/JiraUserInterface';
import { JiraUsersFilterInterface } from '../interfaces/JiraUsersFilterInterface';
import { JiraUser } from '../models/JiraUser';
import { ConfigService } from "./ConfigService";

@Injectable()
export class JiraUserService {
    /**
     * @type {Subject<JiraUserInterface[]>}
     */
    private onLoadJiraUsersSubject$: Subject<JiraUserInterface[]> = new Subject<JiraUserInterface[]>();

    /**
     * @type {Subject<JiraUsersFilterInterface>}
     */
    private onJiraUsersFiltersChangeSubject$: Subject<JiraUsersFilterInterface> = new Subject<JiraUsersFilterInterface>();

    /**
     * @type {JiraUserInterface[]}
     */
    private $jiraUsers: JiraUserInterface[] = [];

    /**
     * @type {JiraUsersFilterInterface}
     */
    private $jiraUsersFilter: JiraUsersFilterInterface;

    /**
     * @type {JiraUsersFilterInterface}
     */
    private $jiraUsersFilterDefault: JiraUsersFilterInterface;

    /**
     * @type {Observable<JiraUserInterface[]>}
     */
    public onLoadJiraUsers$: Observable<JiraUserInterface[]> = this.onLoadJiraUsersSubject$.asObservable();

    /**
     * Set initial configuration values
     * @param {ConfigService} configService
     * @param {ApiService} apiService
     */
    public constructor(
        private configService: ConfigService,
        private apiService: ApiService
    ) {
        this.$jiraUsersFilter = this.$jiraUsersFilterDefault = { };
    }

    public loadJiraUsers(
        applicationId: any,
        filter: boolean = false
    ): Observable<JiraUserInterface[]> {
        let options: JiraUsersFilterInterface = this.$jiraUsersFilterDefault;

        if (filter) {
            options = this.$jiraUsersFilter;
        }

        this.apiService.loadJiraUsers(applicationId, options).subscribe(
            ({ data, error }: { data: any[], error: boolean }) => {
                const array: JiraUserInterface[] = [];

                if (error) {
                    return;
                }

                data.forEach((element) => {
                    array.push(new JiraUser(element));
                });

                this.$jiraUsers = array;
                this.onLoadJiraUsersSubject$.next(this.$jiraUsers);
            }
        );

        return this.onLoadJiraUsers$;
    }

    /**
     * Set new jira users filter, or replace an existing one
     * @param {string|any} name Parameter name
     * @param {any} value Parameter value
     */
    public setJiraUsersFilter(name: string|any, value: any = null) {
        this.onJiraUsersFiltersChangeSubject$.next(this.$jiraUsersFilter);
    }

    /**
     * Reset jira users filter to defaults
     */
    public resetJiraUsersFilter() {
        this.$jiraUsersFilter = this.$jiraUsersFilterDefault;
    }

    /**
     * Get jira users list
     * @returns {JiraApplicationInterface[]}
     */
    get jiraUsers(): JiraUserInterface[] {
        return this.$jiraUsers;
    }

    /**
     * Get current jira users filter
     * @returns {JiraApplicationsFilterInterface}
     */
    get jiraUsersFilter(): JiraUsersFilterInterface {
        return this.$jiraUsersFilter;
    }
}