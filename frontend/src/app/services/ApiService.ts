/**
 * @license
 * Copyright iKantam LLC. All Rights Reserved.
 *
 * Use of this source code is governed by an CC BY-NC-ND 4.0 license that can be
 * found in the LICENSE file at https://creativecommons.org/licenses/by-nc-nd/4.0
 */
import { Injectable } from '@angular/core';
import { ConfigService } from '../services/ConfigService';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { CookieService } from 'ngx-cookie';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { JiraApplicationsFilterInterface } from '../interfaces/JiraApplicationsFilterInterface';
import {JiraUsersFilterInterface} from "../interfaces/JiraUsersFilterInterface";

@Injectable()
export class ApiService {
    /**
     * @type {any}
     */
    private $token: string = null;

    /**
     * @type {any}
     */
    private $subscriptionsPool: { [index: string]: Subscription } = {};

    /**
     * ApiService constructor.
     * @param {Http} http
     * @param {ConfigService} configService
     * @param {CookieService} cookieService
     * @param {Router} router
     */
    public constructor(
        private http: Http,
        private configService: ConfigService,
        private cookieService: CookieService,
        private router: Router
    ) {
        this.$token = this.cookieService.get('jwt_ikantam');
    }

    public loadJiraApplications(options: JiraApplicationsFilterInterface) {
        const params = new URLSearchParams();
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        params.set('filters', JSON.stringify({ }));

        return this.apiCall(() => (
            this.http.get(this.configService.getConfig().api.jiraApplications, {
                search: params,
                headers
            })
        ), 'loadJiraApplications');
    }

    public loadJiraUsers(applicationId: any, options: JiraUsersFilterInterface) {
        const params = new URLSearchParams();
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        params.set('filters', JSON.stringify({ }));

        return this.apiCall(() => (
            this.http.get(this.configService.getConfig().api.jiraUsers.replace('{applicationId}', applicationId), {
                search: params,
                headers
            })
        ), 'loadJiraUsers');
    }

    public loadReports(applicationId: any, dateFrom: any, dateTo: any, userEmail: string, status: string) {
        const params = new URLSearchParams();
        const headers = new Headers();
        this.createAuthorizationHeader(headers);
        params.set('date_from', dateFrom);
        params.set('date_to', dateTo);
        params.set('assignee', userEmail);
        params.set('status', status);

        return this.apiCall(() => (
            this.http.get(this.configService.getConfig().api.reports.replace('{applicationId}', applicationId), {
                search: params,
                headers
            })
        ), 'loadReports');
    }

    public loadAccount() {
        const headers = new Headers();
        this.createAuthorizationHeader(headers);

        return this.apiCall(() => (
            this.http.get(this.configService.getConfig().api.account, {
                search: '',
                headers
            })
        ), 'loadAccount');
    }

    /**
     * Append authorization header
     * @param {Headers} headers
     */
    public createAuthorizationHeader(headers: Headers) {
        headers.append('Authorization', 'Bearer ' + this.$token);
    }

    /**
     * Make api call, handle errors, alerts and validation errors
     * Register errors, alerts and validation errors in AlertService
     * @see AlertService
     * @param {Function} api
     * @param {string} name
     * @returns {Observable<any>}
     */
    private apiCall(api: Function, name: string): Observable<any> {
        const subject$: Subject<any> = new Subject<any>();
        const observable: Observable<any> = subject$.asObservable();

        if (this.$subscriptionsPool.hasOwnProperty(name)) {
            this.$subscriptionsPool[name].unsubscribe();
        }

        this.$subscriptionsPool[name] = api().subscribe(
            (response: Response) => {
                const body = response.json();

                if (response.status != 200) {
                    subject$.next({ data: body.data || {}, error: true, status: response.status });
                } else {
                    subject$.next({ data: body.data || {}, error: false, status: response.status, totals: body.totals || {} });
                }
            },
            (error: Response | any) => {
                if (error instanceof Response) {
                    this.handleError(error);
                }
                subject$.complete();
            },
            () => {
                subject$.complete();
            }
        );

        return observable;
    }

    private handleError(error: Response) {
        if (error.status == 401) {
            window.location.href = 'http://auth.ikantam.com';
        } else if (error.status === 403 || error.status === 405) {
            this.router.navigate(['/forbidden']);
        }
    }
}