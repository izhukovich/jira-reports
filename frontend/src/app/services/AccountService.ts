import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { ApiService } from './ApiService';
import { AccountInterface } from '../interfaces/AccountInterface';
import { Account } from '../models/Account';
import { ConfigService } from "./ConfigService";

@Injectable()
export class AccountService {
    /**
     * @type {Subject<AccountInterface>}
     */
    private onLoadAccountSubject$: Subject<AccountInterface> = new Subject<AccountInterface>();

    /**
     * @type {AccountInterface}
     */
    private $account: AccountInterface;


    /**
     * @type {Observable<AccountInterface>}
     */
    public onLoadAccount$: Observable<AccountInterface> = this.onLoadAccountSubject$.asObservable();

    /**
     * Set initial configuration values
     * @param {ConfigService} configService
     * @param {ApiService} apiService
     */
    public constructor(
        private configService: ConfigService,
        private apiService: ApiService
    ) {
        //
    }

    public loadAccount(): Observable<AccountInterface> {
        this.apiService.loadAccount().subscribe(
            ({ data, error }: { data: any[], error: boolean }) => {

                if (error) {
                    return;
                }

                this.$account = new Account(data);
                this.onLoadAccountSubject$.next(this.$account);
            }
        );

        return this.onLoadAccount$;
    }

    /**
     * Get account
     * @returns {AccountInterface}
     */
    get account(): AccountInterface {
        return this.$account;
    }
}