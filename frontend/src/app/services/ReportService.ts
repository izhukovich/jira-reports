import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { ApiService } from './ApiService';
import { ReportInterface } from '../interfaces/ReportInterface';
import { Report } from '../models/Report';
import { ConfigService } from "./ConfigService";

@Injectable()
export class ReportService {
    /**
     * @type {Subject<JiraUserInterface[]>}
     */
    private onLoadReportsSubject$: Subject<ReportInterface[]> = new Subject<ReportInterface[]>();

    /**
     * @type {ReportInterface[]}
     */
    private $reports: ReportInterface[] = [];

    private $totals: any[] = [];

    /**
     * @type {Observable<JiraUserInterface[]>}
     */
    public onLoadReports$: Observable<ReportInterface[]> = this.onLoadReportsSubject$.asObservable();

    /**
     * Set initial configuration values
     * @param {ConfigService} configService
     * @param {ApiService} apiService
     */
    public constructor(
        private configService: ConfigService,
        private apiService: ApiService
    ) { }

    public loadReports(
        applicationId: any,
        dateFrom: string,
        dateTo: string,
        userEmail: string,
        status: string
    ): Observable<ReportInterface[]> {
        this.apiService.loadReports(applicationId, dateFrom, dateTo, userEmail, status).subscribe(
            ({ data, error, totals }: { data: any[], error: boolean, totals: any[] }) => {
                const array: ReportInterface[] = [];

                if (error) {
                    return;
                }

                data.forEach((element) => {
                    array.push(new Report(element));
                });

                this.$reports = array;
                this.$totals = totals;
                this.onLoadReportsSubject$.next(this.$reports);
            }
        );

        return this.onLoadReports$;
    }

    /**
     * Get reports list
     * @returns {ReportInterface[]}
     */
    get reports(): ReportInterface[] {
        return this.$reports;
    }

    /**
     * Get totals
     * @returns {any[]}
     */
    get totals(): any[] {
        return this.$totals;
    }
}