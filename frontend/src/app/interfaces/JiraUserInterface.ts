export interface JiraUserInterface {
    name: string,
    emailAddress: string,
    avatarUrls: any[],
    displayName: string,
    timeZone: string,
    serialize(): any;
}