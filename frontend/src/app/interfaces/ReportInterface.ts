export interface ReportInterface {
    key: string,
    summary: string,
    description: string,
    assignee: any[],
    estimated_time: number,
    time_spent: number,
    status: any[],
    updated_at: string,
    created_at: string,
    resolved: string
}