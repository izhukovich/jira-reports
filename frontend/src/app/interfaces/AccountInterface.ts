export interface AccountInterface {
    name: string,
    role: string,
    serialize(): any;
}