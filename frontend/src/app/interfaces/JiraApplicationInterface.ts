export interface JiraApplicationInterface {
    id: number,
    name: string,
    serialize(): any;
}