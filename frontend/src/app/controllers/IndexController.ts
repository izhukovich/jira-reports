import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AbstractController } from './AbstractController';
import { JiraApplicationService } from "../services/JiraApplicationService";
import { JiraApplicationInterface } from "../interfaces/JiraApplicationInterface";

@Component({
    selector: 'application-index',
    templateUrl: '../templates/controllers/index.html'
})
export class IndexController extends AbstractController implements OnInit, OnDestroy {

    public jiraApplications: JiraApplicationInterface[] = [];
    public loading: boolean = true;
    private $subscriptions: Subscription[] = [];

    public constructor(
        protected $route: ActivatedRoute,
        protected $router: Router,
        protected $jiraApplicationsService: JiraApplicationService
    ) {
        super(
            $route,
            $router
        );
    }


    public ngOnInit() {
        this.subscribeToJiraApplications();
        this.$jiraApplicationsService.loadJiraApplications();
    }

    public ngOnDestroy() {
        this.$subscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        });
    }

    private subscribeToJiraApplications() {
        this.jiraApplications = this.$jiraApplicationsService.jiraApplications;
        this.$subscriptions.push(
            this.$jiraApplicationsService.onLoadJiraApplications$.subscribe((jiraApplications: JiraApplicationInterface[]) => {
                this.jiraApplications = jiraApplications;
                this.loading = false;
            })
        );
    }
}
