import { ActivatedRoute, Router } from '@angular/router';

export class AbstractController {
    public constructor(
        protected route: ActivatedRoute,
        protected router: Router
    ) { }
}