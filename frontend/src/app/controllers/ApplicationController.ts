import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { AbstractController } from '../controllers/AbstractController';

@Component({
    selector: 'application',
    templateUrl: '../templates/controllers/application.html'
})
export class ApplicationController extends AbstractController {
    public constructor(
        protected $route: ActivatedRoute,
        protected $router: Router,
    ) {
        super(
            $route,
            $router
        );
    }
}
