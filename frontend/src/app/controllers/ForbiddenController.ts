import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AbstractController } from './AbstractController';

@Component({
    selector: 'application-forbidden',
    templateUrl: '../templates/controllers/forbidden.html'
})
export class ForbiddenController extends AbstractController implements OnInit, OnDestroy {

    public constructor(
        protected $route: ActivatedRoute,
        protected $router: Router,
    ) {
        super(
            $route,
            $router
        );
    }


    public ngOnInit() {
        //
    }

    public ngOnDestroy() {
        //
    }
}
