import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AbstractController } from './AbstractController';
import { JiraUserService } from '../services/JiraUserService';
import { JiraUserInterface } from '../interfaces/JiraUserInterface';
import { ReportService } from '../services/ReportService';
import { ReportInterface } from '../interfaces/ReportInterface';

import { DatepickerModule } from 'ng2-bootstrap/ng2-bootstrap';

@Component({
    selector: 'application-report',
    templateUrl: '../templates/controllers/report.html'
})
export class ReportController extends AbstractController implements OnInit, OnDestroy {

    public jiraUsers: JiraUserInterface[] = [];
    public loading: boolean = false;
    private $subscriptions: Subscription[] = [];

    public reports: ReportInterface[] = [];
    public totals: any[] = [];

    public selectedUser: JiraUserInterface;
    public dateFrom:Date = new Date();
    public dateTo:Date = new Date();
    public selectedStatus: string = '';

    public constructor(
        protected $route: ActivatedRoute,
        protected $router: Router,
        protected $jiraUserService: JiraUserService,
        protected $reportService: ReportService
    ) {
        super(
            $route,
            $router
        );
    }


    public ngOnInit() {
        this.subscribeToJiraUsers();
        this.subscribeToReports();
    }

    public ngOnDestroy() {
        this.$subscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        });
    }

    public selectUserForReport(user: JiraUserInterface) {
        this.selectedUser = user;
    }

    public unselectUserForReport() {
        this.reports = [];
        this.selectedUser = null;
    }
    
    public selectStatus(status) {
        this.selectedStatus = status;
    }

    public getReport() {
        const applicationId = this.$route.params['_value']['applicationId'];
        const userEmail = this.selectedUser.emailAddress;

        const dateFromMonth = this.dateFrom.getMonth() + 1;
        const dateFrom = this.dateFrom.getFullYear() + '-' + dateFromMonth + '-' + this.dateFrom.getDate();

        const dateToMonth = this.dateTo.getMonth() + 1;
        const dateTo = this.dateTo.getFullYear() + '-' + dateToMonth + '-' + this.dateTo.getDate();

        const status = this.selectedStatus;

        this.reports = [];
        this.loading = true;

        this.$reportService.loadReports(applicationId, dateFrom, dateTo, userEmail, status);
    }

    private subscribeToJiraUsers() {
        this.jiraUsers = this.$jiraUserService.jiraUsers;
        this.$subscriptions.push(
            this.$jiraUserService.onLoadJiraUsers$.subscribe((jiraUsers: JiraUserInterface[]) => {
                this.jiraUsers = jiraUsers;
                this.loading = false;
            })
        );
    }

    private subscribeToReports() {
        this.reports = this.$reportService.reports;
        this.$subscriptions.push(
            this.$reportService.onLoadReports$.subscribe((reports: ReportInterface[]) => {
                this.reports = reports;
                this.totals = this.$reportService.totals;
                this.loading = false;
            })
        );
    }
}
