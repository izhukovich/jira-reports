<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ApplicationsService;

class ApplicationsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ApplicationsService::class, function ($app) {
            return new ApplicationsService();
        });
    }
}
