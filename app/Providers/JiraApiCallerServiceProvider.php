<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\JiraApiCallerService;

class JiraApiCallerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(JiraApiCallerService::class, function ($app) {
            return new JiraApiCallerService();
        });
    }
}
