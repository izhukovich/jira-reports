<?php

namespace App\Services;

use App\Interfaces\ApplicationsServiceInterface;
use App\Models\Application;

class ApplicationsService implements ApplicationsServiceInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        $application = Application::query()
            ->firstOrFail()
            ->where(['id' => $id])
            ->get();

        return $application[0];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList()
    {
        $data = [
            'data' => Application::all()
        ];

        return $data;
    }

    /**
     * @param $data
     * @return Application
     */
    public function create($data)
    {
        $application = new Application();
        $application = $this->dataToModel($data, $application);
        $application->save();

        return $application;
    }

    /**
     * @param $id
     * @param $data
     * @return Application
     */
    public function update($id, $data)
    {
        $application = Application::query()
            ->firstOrFail()
            ->where(['id' => $id])
            ->get();

        $application = $this->dataToModel($data, $application);
        $application->save();

        return $application;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Application::destroy($id);

        return ['data' => 'Deleted'];
    }

    /**
     * @param $data array
     * @param $model Application
     * @return Application
     */
    protected function dataToModel($data, $model)
    {
        if ($data['name']) {
            $model->name = $data['name'];
        }
        if ($data['jira_email'] && $data['jira_password']) {
            $model->key = base64_encode($data['jira_email'] . ':' . $data['jira_password']);
        }

        return $model;
    }

}
