<?php

namespace App\Services;

use App\Interfaces\JiraApiCallseServiceInterface;
use App\Models\Application;
use Illuminate\Http\Request;

class JiraApiCallerService implements JiraApiCallseServiceInterface
{
    const URL_USERS = 'rest/api/2/user/search?username=%';
    const URL_ISSUES = 'rest/api/2/search';

    public function getApplicationUsers($applicationId)
    {
        $data = [
            'data' => $this->getUsers($applicationId)
        ];

        return $data;
    }

    public function getApplicationIssues($applicationId, Request $request)
    {
        $issues = $this->getIssues($applicationId, $request);
        $totalEstimated = $this->calculateTotalEstimated($issues);
        $totalSpent = $this->calculateTotalSpent($issues);

        $data = [
            'data' => $issues,
            'totals' => [
                'total_estimated' => $totalEstimated,
                'total_spent' => $totalSpent
            ]
        ];

        return $data;
    }

    public function getUsers($applicationId)
    {
        $application = $this->getApplication($applicationId);

        if (!$application) {
            return [];
        }

        $headers = $this->initAuthorizationHeaders($application->key);

        $url = 'https://' . $application->name . '.atlassian.net/' . self::URL_USERS;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $json = curl_exec($ch);

        $result = json_decode($json);

        return $result;
    }

    public function getIssues($applicationId, Request $request) {
        $project = $request->get('project');
        $status = $request->get('status');
        $assignee = $request->get('assignee');
        $dateFrom = $request->get('date_from');
        $dateTo = $request->get('date_to');
        $orderBy = $request->get('order_by');
        $orderDir = $request->get('order_dir');

        $data = $this->findIssuesByQuery(
                $applicationId,
                $project,
                $status,
                $assignee,
                $dateFrom,
                $dateTo,
                $orderBy,
                $orderDir
            );

        return $data;
    }

    protected function getApplication($applicationId)
    {
        $application = Application::query()
            ->firstOrFail()
            ->where(['id' => $applicationId])
            ->get();

        $application = $application[0];

        return $application;
    }

    /**
     * @param $applicationId
     * @param null $project
     * @param null $status
     * @param null $assignee
     * @param null $dateFrom
     * @param null $dateTo
     * @param null $orderBy
     * @param null $orderDir
     * @return array
     */
    protected function findIssuesByQuery(
        $applicationId,
        $project = null,
        $status = null,
        $assignee = null,
        $dateFrom = null,
        $dateTo = null,
        $orderBy = null,
        $orderDir = null
    ) {
        $application = $this->getApplication($applicationId);

        if (!$application) {
            return [];
        }

        $headers = $this->initAuthorizationHeaders($application->key);

        $jql = '';

        if ($project) {
            $jql = $this->addToJql($jql, 'project = "' . $project . '"');
        }

        if ($status) {
            $jql = $this->addToJql($jql, '(status = "' . $status . '")');
        }

        if ($assignee) {
            $jql = $this->addToJql($jql, '(assignee = "' . $assignee . '")');
        }

        if ($dateFrom && !$dateTo) {
            $jql = $this->addToJql($jql, '(updated >= "' . $dateFrom . '")');
        }

        if ($dateTo && !$dateFrom) {
            $jql = $this->addToJql($jql, '(updated <= "' . $dateTo . '")');
        }

        if ($dateTo && $dateFrom) {
            $jql = $this->addToJql($jql, '(updated >= "' . $dateFrom . '" AND updated <= "' . $dateTo . '")');
        }

        if ($orderBy && $orderDir) {
            $jql = $jql . ' ORDER BY ' . $orderBy . ' ' . $orderDir;
        }

        $url = 'https://' . $application->name . '.atlassian.net/' . self::URL_ISSUES . '?jql=' . urlencode($jql) . '&maxResults=9999';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $json = curl_exec($ch);

        $result = json_decode($json);

        return $this->executeIssues($result);
    }

    protected function executeIssues($result)
    {
        $data = [];
        foreach ($result->issues as $issue) {
            $issueData = $this->executeFromIssue($issue);
            $data[] = $issueData;
        }

        return $data;
    }

    /**
     * @param $issue object
     * @return array
     */
    protected function executeFromIssue($issue)
    {
        $data = [
            'key' => $issue->key,
            'summary' => $issue->fields->summary,
            'description' => $issue->fields->description,
            'assignee' => $issue->fields->assignee,
            'estimated_time' => $this->formatSecondsToHours($issue->fields->timeoriginalestimate),
            'time_spent' => $this->formatSecondsToHours($issue->fields->timespent),
            'status' => $issue->fields->status,
            'created_at' => $issue->fields->created,
            'updated_at' => $issue->fields->updated,
            'resolved' => $issue->fields->resolutiondate
        ];

        return $data;
    }

    protected function calculateTotalEstimated($issues)
    {
        $totalEstimated = 0;

        foreach ($issues as $issue) {
           $totalEstimated += $issue['estimated_time'];
        }

        return $totalEstimated;
    }

    protected function calculateTotalSpent($issues)
    {
        $totalSpent = 0;

        foreach ($issues as $issue) {
            $totalSpent += $issue['time_spent'];
        }

        return $totalSpent;
    }

    /**
     * @param $jql
     * @param $string
     * @return string
     */
    protected function addToJql($jql, $string)
    {
        if (!empty($jql)) {
            $jql = $jql . ' AND ';
        }

        return $jql . $string;
    }

    /**
     * @param $seconds
     * @return float
     */
    protected function formatSecondsToHours($seconds)
    {
        return floor($seconds / 3600);
    }

    protected function initAuthorizationHeaders($key)
    {
        $headers = [
            'Content-Type:application/json',
            'Authorization: Basic '. $key
        ];

        return $headers;
    }
}
