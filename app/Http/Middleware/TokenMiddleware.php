<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Exception;

class TokenMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  $roles
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $roles)
    {
        $token = $request->headers->get('Authorization');
        if (!$token) {
            return response(null, 401);
        }

        $token = str_replace('Bearer ', '', $token);

        try {
            $data = JWT::decode($token,config('token.secret'), [config('token.algo')]);
            $roles = explode('|', $roles);
            if (!isset($data->user->roles->auth) || !in_array($data->user->roles->auth, $roles)) {
                return response(null, 403);
            }
        } catch (Exception $exception) {
            return response($exception->getMessage(), 401);
        }

        return $next($request);
    }
}
