<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ApplicationsService;
use Illuminate\Validation\ValidationException;

class ApplicationsController extends Controller
{
    protected $applicationsService;

    /**
     * ApplicationsController constructor
     * @param ApplicationsService $applicationsService
     */
    public function __construct(ApplicationsService $applicationsService)
    {
        $this->applicationsService = $applicationsService;
    }

    public function get($id)
    {
        return $this->applicationsService->get($id);
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->applicationsService->getList();
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:applications',
            'jira_email' => 'required',
            'jira_password' => 'required'
        ]);

        return $this->applicationsService->create($request->all());
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'jira_email' => 'email|unique:applications,jira_email,' . $id,
        ]);

        return $this->applicationsService->update($id, $request->all());
    }

    public function delete($id)
    {
        return $this->applicationsService->delete($id);
    }
}
