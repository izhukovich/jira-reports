<?php

namespace App\Http\Controllers;

use App\Services\JiraApiCallerService;
use App\Services\JiraService;
use Illuminate\Http\Request;
use App\Services\ApplicationsService;
use Illuminate\Validation\ValidationException;

class IssuesController extends Controller
{
    protected $applicationsService;
    protected $jiraApiCallerService;

    /**
     * ApplicationsController constructor
     * @param ApplicationsService $applicationsService
     * @param JiraApiCallerService $jiraApiCallerService
     */
    public function __construct(
        ApplicationsService $applicationsService,
        JiraApiCallerService $jiraApiCallerService
    ) {
        $this->applicationsService = $applicationsService;
        $this->jiraApiCallerService = $jiraApiCallerService;
    }

    /**
     * @param $applicationId
     */
    public function getApplicationIssues($applicationId, Request $request)
    {
        return $this->jiraApiCallerService->getApplicationIssues($applicationId, $request);
    }

    public function getApplicationUsers($applicationId)
    {
        return $this->jiraApiCallerService->getApplicationUsers($applicationId);
    }
}
