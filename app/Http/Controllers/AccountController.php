<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Exception;

class AccountController extends Controller
{
    public function get(Request $request)
    {
        $token = $request->headers->get('Authorization');
        if (!$token) {
            return response(null, 401);
        }

        $token = str_replace('Bearer ', '', $token);

        try {
            $data = JWT::decode($token,config('token.secret'), [config('token.algo')]);
            $user = $data->user;

            $data = [
                'success' => true,
                'data' => $user,
            ];
        } catch (Exception $exception) {
            $data = [
                'success' => false,
                'data' => [],
                'message' => $exception->getMessage()
            ];
        }
        return $data;
    }
}
