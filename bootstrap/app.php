<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
 * Create The Application
 */

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();
$app->withEloquent();

/*
 * Configs
 */
$app->configure('token');

/*
 * Register Container Bindings
 */

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
 * Register Middleware
 */
$app->routeMiddleware([
    'token' => App\Http\Middleware\TokenMiddleware::class,
    'role' => \Zizaco\Entrust\Middleware\EntrustRole::class,
]);

$app->middleware([
    App\Http\Middleware\CorsMiddleware::class
]);

/*
 * Register Service Providers
 */

$app->register(App\Providers\ApplicationsServiceProvider::class);
$app->register(App\Providers\JiraApiCallerServiceProvider::class);

/*
 * Load The Application Routes
 */

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../routes/web.php';
});

return $app;
