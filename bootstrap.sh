sudo apt-add-repository ppa:ondrej/php
apt-get update
apt-get install -y apache2
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password rootpass'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password rootpass'
sudo apt-get -y install mysql-server libapache2-mod-auth-mysql php7.0-mysql
sudo apt-get -y install php7.0 php7.0-mysql php7.0-curl php7.0-mbstring php7.0-xml php7.0-zip php7.0-xdebug php7.0-gd
sudo apt-get -y install git
sudo apt-get -y install npm
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo npm install -g angular-cli

# Remove /var/www default
rm -rf /var/www

# Symlink /vagrant to /var/www
ln -fs /vagrant/public /var/www

# Create virtual host
cat > /etc/apache2/sites-enabled/000-default.conf << EOF
<VirtualHost *:80>
    ServerName localhost
    ServerAdmin admin@ikantam.com
    DocumentRoot /var/www
    <Directory "/var/www">
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Allow from All
        Require all granted
    </Directory>
</VirtualHost>
EOF

# Enable mod_rewrite
sudo a2enmod rewrite

# Restart apache
sudo service apache2 restart

# Download composer
curl -Ss https://getcomposer.org/installer | php
sudo mv composer.phar /usr/bin/composer

# Update dependencies
cd /vagrant
composer update

cat > /vagrant/.env << EOF
APP_ENV=local
APP_DEBUG=true
APP_KEY=d3dsadasd32hyhkdt7fdghvcbc

APP_LOCALE=en
APP_FALLBACK_LOCALE=en

DB_CONNECTION=mysql
DB_HOST=localhost
DB_DATABASE=jira_reports
DB_USERNAME=root
DB_PASSWORD=rootpass

CACHE_DRIVER=array
SESSION_DRIVER=array
QUEUE_DRIVER=database

# MAIL_DRIVER=smtp
# MAIL_HOST=mailtrap.io
# MAIL_PORT=2525
# MAIL_USERNAME=null
# MAIL_PASSWORD=null
# MAIL_FROM_ADDRESS=null
# MAIL_FROM_NAME=null

# FILESYSTEM_DRIVER=local
# FILESYSTEM_CLOUD=s3

# S3_KEY=null
# S3_SECRET=null
# S3_REGION=null
# S3_BUCKET=null

# RACKSPACE_USERNAME=null
# RACKSPACE_KEY=null
# RACKSPACE_CONTAINER=null
# RACKSPACE_REGION=null
EOF

# Create database if not exists
mysql -uroot -prootpass -e "CREATE DATABASE IF NOT EXISTS jira_reports"

# Update database and import data
sh bin/orm.sh update-schema
sh bin/orm.sh import:fixtures

