<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});


$app->group(['middleware' => ['token:user|admin|superadmin']], function () use ($app) {
    //jira applications
    $app->get('/api/applications', 'ApplicationsController@getList');
    $app->post('/api/applications', 'ApplicationsController@create');
    $app->get('/api/applications/{id}', 'ApplicationsController@get');
    $app->put('/api/applications/{id}', 'ApplicationsController@update');
    $app->delete('/api/applications/{id}', 'ApplicationsController@delete');
    //issues
    $app->get('/api/issues/{applicationId}', 'IssuesController@getApplicationIssues');
    $app->get('/api/users/{applicationId}', 'IssuesController@getApplicationUsers');
});

$app->get('/api/account', 'AccountController@get');
