# JIRA Reports

## Requirements

- [Git](https://git-scm.com/downloads)
- [Composer](https://getcomposer.org/download/)
- [optional] [Vagrant](https://www.vagrantup.com/downloads.html) (to run app on local machine)
- [optional] [VirtualBox](https://www.virtualbox.org/) (for vagrant only)
- [Node & Npm](https://nodejs.org/en/download/)
- [PHP](http://php.net/manual/ru/install.php) >= 7.0

## Frontend
- jump to the `frontend` directory
- run `ng build --prod` or `ng build --dev` to build production or development app

## Backend
- jump to the root directory of the app
- run `composer install`